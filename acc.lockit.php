<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**
 * ExpressionEngine - by EllisLab
 *
 * @package		ExpressionEngine
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2003 - 2011, EllisLab, Inc.
 * @license		http://expressionengine.com/user_guide/license.html
 * @link		http://expressionengine.com
 * @since		Version 2.0
 * @filesource
 */
 
// ------------------------------------------------------------------------
 
/**
 * Lock field Accessory
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Accessory
 * @author		Pete Eveleigh
 * @link		http://www.fantasticmachine.co.uk
 */
 
class Lockit_acc {
	
	public $name			= 'LockIt';
	public $id				= 'lockit';
	public $version			= '1.0';
	public $description		= 'Locks a field to prevent its contents being changed';
	public $sections		= array();
	


	/**
	 * Constructor
	 */
	function __construct()
	{
		$this->EE =& get_instance();
	}


	/**
	 * Set Sections
	 */
	function set_sections()
	{

		// hide accessory
		$this->sections[] = '<script>$("#accessoryTabs a.lockit").parent().remove();</script>';

		$this->EE->cp->add_to_head('
			<script>
				$(document).ready( function() {

					var fields = $.find("label>span:contains(\'Lockit\')");

					$(fields).each(function(){
						$(this).parent().siblings("div").find("input").each(function(){
							lockfield($(this));
						});
					})
					
					
				});


				function lockfield(f){
					f.attr("readonly","readonly");
					var c = $(f).val();
					$("<p>"+c+"</p>").insertAfter(f);
					f.hide();
				}
			</script>
		');

	}

	// ----------------------------------------------------------------
	
}
 
/* End of file acc.lock_field.php */
/* Location: /system/expressionengine/third_party/lock_field/acc.lock_field.php */