# Lockit

Lockit is an Accessory for ExpressionEngine 2.x and will allow you to lock down any fields that you don't want to be editable once populated.

## Installation

To install, simply put the lockit directory inside your system/third_party/ directory then activate within your ExpressionEngine control panel.


## Instructions for use

To lock a field, just add "Lockit" to the field's label (not its short name).

For example:

If you have a field labelled "Favourite Cheese" then just change its name to "Favourite Cheese Lockit"